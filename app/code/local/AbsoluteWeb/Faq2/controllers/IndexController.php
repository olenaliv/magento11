<?php
/**
 * Absolute Web Intellectual Property
 *
 * @copyright    Copyright © 1999-2019 Absolute Web, Inc. (http://www.absoluteweb.com)
 * @author       Absolute Web
 * @license      http://www.absoluteweb.com/license-agreement/  Single domain license
 * @terms of use http://www.absoluteweb.com/terms-of-use/
 */

class AbsoluteWeb_Faq2_IndexController extends Mage_Core_Controller_Front_Action
{

    private function _initLayout()
    {
        $this->loadLayout()->renderLayout();
    }

    /**
     * This method is output all questions and answers to them
     * For example you may visit the following URL http://example.com/frontName/index/getAllFaq
     */
    public function getAllFaqAction()
    {
        $this->_initLayout();
    }

    /**
     * Render form to add new faq
     */
    public function addNewFaqAction()
    {
        // the same form is used to create and edit
        $model = Mage::getModel('absoluteweb_faq2/faq');
        Mage::register('aws_faq_model', $model);
        $this->_initLayout();
    }

    /**
     * Render form to edit faq
     */
    public function editFaqByIdAction()
    {
        $id = $this->getRequest()->getParam('id', null);

        if(!$id){
            //error
            $this->_initLayout();
            return ;
        }

        $model = Mage::getModel('absoluteweb_faq2/faq')
            ->load($id);

        if(!$model->getId()) {
            //error
            $this->_initLayout();
            return ;
        }

        Mage::register('aws_faq_model', $model);
        $this->_initLayout();
    }

    public function saveAction()
    {
        $faqObject = Mage::getModel('absoluteweb_faq2/faq');
        $id = $this->getRequest()->getParam('question_id', null);

        if ((!intval($id)) && ($id!=null)){
            $this->_getErrorMessage('invalid id');
            $this->_redirect('crud/index/getAllFaq');
            return;
            /** @var Mage_Core_Model_Session $session */
        }

        $data = array(
            'question' => $this->getRequest()->getParam('question_enter',''),
            'answer' => $this->getRequest()->getParam('answer_enter', ''),
            'date' => date('Y-m-d H:i:s')
        );

        if ($id){
            $faqObject = $faqObject->load($id);
        }

        $faqObject->addData($data)
            ->save();

        $msg = $this->__('The item had been edited');

        if ($faqObject->isObjectNew()){
            $msg = $this->__('The new item had been added');
        }

        $this->_getSuccessMessage($msg);

        $this->_redirect('crud/index/getAllFaq');
    }

    /**
     * Delete faq by id
     */
    public function deleteAction()
    {
        /**
         * @todo get id sent by url and delete faq
         *       add message by using session
         */
        $id = $this->getRequest()->getParam('id');
        $faqObject = Mage::getModel('absoluteweb_faq2/faq');
        $faqObject->load($id)->delete();
        $this->_redirect('crud/index/getAllFaq');
    }

    private function _getSuccessMessage($msg)
    {
        return Mage::getSingleton('core/session')
            ->addSuccess($this->__($msg));
    }

    private function _getErrorMessage($msg)
    {
        return Mage::getSingleton('core/session')
            ->addError($this->__($msg));
    }

}
