<?php
/**
 * Absolute Web Intellectual Property
 *
 * @copyright    Copyright © 1999-2019 Absolute Web, Inc. (http://www.absoluteweb.com)
 * @author       Absolute Web
 * @license      http://www.absoluteweb.com/license-agreement/  Single domain license
 * @terms of use http://www.absoluteweb.com/terms-of-use/
 */

class AbsoluteWeb_Faq2_Block_Faq extends Mage_Core_Block_Template
{

    /**
     * Retrieve all faq sorted by date
     *
     * @return AbsoluteWeb_Faq2_Model_Resource_Faq_Collection
     */
    public function getAllFaq()
    {
        /**
         * @todo get collection of all FAQ
         */
        return Mage::getResourceModel('absoluteweb_faq2/faq_collection')->setOrder('date','DESC');

    }

    /**
     * Retrieve faq by id
     *
     * @return AbsoluteWeb_Faq2_Model_Faq
     */
    public function getFaqById()
    {
        $model = Mage::registry('aws_faq_model');

        if(!$model) {
            return Mage::getModel('absoluteweb_faq2/faq');
        }
        return $model;
    }

}