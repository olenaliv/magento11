<?php
/**
 * Absolute Web Intellectual Property
 *
 * @copyright    Copyright © 1999-2019 Absolute Web, Inc. (http://www.absoluteweb.com)
 * @author       Absolute Web
 * @license      http://www.absoluteweb.com/license-agreement/  Single domain license
 * @terms of use http://www.absoluteweb.com/terms-of-use/
 */

class AbsoluteWeb_News_Model_News extends Mage_Core_Model_Abstract
{

    protected function _construct()
    {
        parent::_construct();

        $this->_init('absoluteweb_news/news');
    }

    protected function setCreateDate()
    {
        $data = array(
            'create_date' => date('Y-m-d H:i:s')
        );
        $this->addData($data);
        return $this;
    }

    protected function setUpdateDate()
    {
        $data = array(
            'update_date' => date('Y-m-d H:i:s')
        );
        $this->addData($data);
        return $this;
    }

    protected function _beforeSave()
    {
        $helper = Mage::helper('absoluteweb_news');
        if ($this->isObjectNew()) {
            $this->setCreateDate();
        }
        $this->setUpdateDate();

        if (!Zend_Validate::is($this->getTitle(), 'NotEmpty')) {
            Mage::throwException($helper->__('Please enter the title.'));
        }

        if (!Zend_Validate::is($this->getContent(), 'NotEmpty')) {
            Mage::throwException($helper->__('Please enter the content.'));
        }

        if (!Zend_Validate::is($this->getImage(), 'NotEmpty')) {
            Mage::throwException($helper->__('Please enter the image.'));
        }

        return ;
    }
}
