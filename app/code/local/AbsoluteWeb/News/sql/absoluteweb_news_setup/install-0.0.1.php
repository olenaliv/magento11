<?php
$installer = $this;
$installer->startSetup();

$table = $installer->getConnection()
    ->newTable($installer->getTable('absoluteweb_news/news'))
    ->addColumn('entity_id',Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned' => true,
        'nullable' => false,
        'primary' => true,
        'identity' => true,
    ), 'Entity ID')
    ->addColumn('title',Varien_Db_Ddl_Table::TYPE_VARCHAR,255,array(
        'nullable' => false,
    ), 'AbsoluteWeb_News title')
    ->addColumn('image',Varien_Db_Ddl_Table::TYPE_VARCHAR,255,array(
        'nullable' => true,
    ), 'AbsoluteWeb_News image')
    ->addColumn('content',Varien_Db_Ddl_Table::TYPE_TEXT,'16m',null,array(
        'nullable' => false,
    ), 'AbsoluteWeb_News content')
    ->addColumn('create_date',Varien_Db_Ddl_Table::TYPE_TIMESTAMP,null,array(
        'nullable' => false,
    ), 'AbsoluteWeb_News create_date')
    ->addColumn('update_date',Varien_Db_Ddl_Table::TYPE_TIMESTAMP,null,array(
        'nullable' => false,
    ), 'AbsoluteWeb_News update_date')
    ->addColumn('publish',Varien_Db_Ddl_Table::TYPE_SMALLINT,null,array(
        'unsigned' => true,
        'nullable' => false
    ), 'AbsoluteWeb_News publish');
$installer->getConnection()->createTable($table);
$installer->endSetup();