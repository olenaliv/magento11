<?php
/**
 * Absolute Web Intellectual Property
 *
 * @copyright    Copyright © 1999-2019 Absolute Web, Inc. (http://www.absoluteweb.com)
 * @author       Absolute Web
 * @license      http://www.absoluteweb.com/license-agreement/  Single domain license
 * @terms of use http://www.absoluteweb.com/terms-of-use/
 */

class AbsoluteWeb_News_Adminhtml_NewsController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->loadLayout();
        $this->_setActiveMenu('absoluteweb_news');
        $this->_title($this->__("Admin News"));
        $this->renderLayout();
    }

    protected function _initNews()
    {
        $helper = Mage::helper('absoluteweb_news');
        $this->_title($helper->__('AbsoluteWeb'))->_title($helper->__('News'));
        $model = Mage::getModel('absoluteweb_news/news');
        $newsId = $this->getRequest()->getParam('id');

        if (!is_null($newsId)) {
            $model->load($newsId);
        }
        Mage::register('current_news', $model);
    }

    public function newAction()
    {
        $this->_initNews();
        $this->loadLayout();
        $this->_addContent(
            $this->getLayout()->createBlock('absoluteweb_news/adminhtml_news_edit',  'edit'),
            $this->getLayout()->createBlock('absoluteweb_news/adminhtml_news_edit_form',  'new')
        );

        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }

        $this->_setActiveMenu('absoluteweb_news');
        $this->renderLayout();
    }

    public function editAction()
    {
        $this->_forward('new');
    }

    public function saveAction()
    {
        $newsObject = Mage::getModel('absoluteweb_news/news');
        $id = $this->getRequest()->getParam('entity_id_enter', null);
        try{
            if ((!intval($id)) && ($id!=null)) {
                $this->_setErrorMessage('invalid id');
                $this->_redirect('*/*/index');
                return;
            }

            $data = array(
                'title' => $this->getRequest()->getParam('title_enter', ''),
                'image' => $this->getRequest()->getParam('image_enter', ''),
                'content' => $this->getRequest()->getParam('content_enter', ''),
                'publish' => $this->getRequest()->getParam('publish_enter', ''),
            );

            if ($id){
                $newsObject = $newsObject->load($id);
            }

            $newsObject->addData($data)->save();
            $msg = $this->__('The item had been edited');

            if ($newsObject->isObjectNew()){
                $msg = $this->__('The new item had been added');
            }
            $this->_setSuccessMessage($msg);
            $this->_redirect('*/*/index');
        } catch (Mage_Core_Exception $e){
            $this->_setErrorMessage($e->getMessage());
            $this->_redirect('*/*/index');
        } catch (Exception $e){
            $this->_setErrorMessage("something is wrong");
            Mage::log($e->getMessage(), 1);
            $this->_redirect('*/*/index');
        }
    }

    public function deleteAction()
    {
        $id = $this->getRequest()->getParam('id');

        if ((!intval($id)) && ($id!=null)) {
            $this->_setErrorMessage('invalid id');
            $this->_redirect('*/news/index');
            return;
        }

        try {
            $this->_currentDelete($id);
            $this->_setSuccessMessage( 'Total of %d record(s) were successfully deleted');
        } catch (Exception $e){
            $this->_setErrorMessage($e->getMessage());
        }
        $this->_redirect('*/news/index');
    }

    public function deleteMassAction()
    {
        $requestIds = $this->getRequest()->getParam('id');

        try {
            if (!is_array($requestIds)) {
                $this->_setErrorMessage('Please select request(s)');
                $this->_redirect('*/*/index');
            }
            foreach ($requestIds as $reqtId) {
                $this->_currentDelete($reqtId);
            }
            $this->_setSuccessMessage( 'Your records successfully deleted');
        } catch (Exception $e) {
            $this->_setErrorMessage($e->getMessage());
        }

        $this->_redirect('*/*/index');
    }

    private function _setSuccessMessage($msg)
    {
        return Mage::getSingleton('core/session')
            ->addSuccess($this->__($msg));
    }

    private function _setErrorMessage($msg)
    {
        return Mage::getSingleton('core/session')
            ->addError($this->__($msg));
    }

    private function _currentDelete($id)
    {
        Mage::getModel('absoluteweb_news/news')->load($id)->delete();
    }

}
