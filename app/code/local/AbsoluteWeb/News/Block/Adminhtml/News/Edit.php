<?php
/**
 * Absolute Web Intellectual Property
 *
 * @copyright    Copyright :copyright: 1999-2019 Absolute Web, Inc. (http://www.absoluteweb.com)
 * @author       Absolute Web
 * @license      http://www.absoluteweb.com/license-agreement/  Single domain license
 * @terms of use http://www.absoluteweb.com/terms-of-use/
 */

class AbsoluteWeb_News_Block_Adminhtml_News_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{

    protected $_blockGroup = 'absoluteweb_news/news';

    public function __construct()
    {
        parent::__construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'absoluteweb_news';
        $this->_controller = 'adminhtml_news';
        $this->_mode = 'edit';
        $this->_headerText = $this->getHeaderText();
        $this->_updateButton('save', 'label', Mage::helper('absoluteweb_news')->__('Save'));
        $this->_updateButton('delete', 'label', Mage::helper('absoluteweb_news')->__('Delete'));
        if (!$this->_getModel()->getId()) {
            $this->_removeButton('delete');
        }
    }

    public function getHeaderText()
    {
        if ($this->_getModel() && $this->_getModel()->getId()) {
            return Mage::helper('absoluteweb_news')
                ->__("Edit '%s'", $this->htmlEscape($this->_getModel()->getTitle()));
        }
        return Mage::helper('absoluteweb_news')->__('Add');
    }

    private function _getModel()
    {
        $model = Mage::registry('current_news');
        if (!$model || !($model instanceof AbsoluteWeb_News_Model_News)) {
            Mage::throwException('Model is not good');
        }
        return $model;
    }
}