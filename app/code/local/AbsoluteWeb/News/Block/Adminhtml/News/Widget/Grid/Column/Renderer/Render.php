<?php
/**
 * Absolute Web Intellectual Property
 *
 * @copyright    Copyright :copyright: 1999-2019 Absolute Web, Inc. (http://www.absoluteweb.com)
 * @author       Absolute Web
 * @license      http://www.absoluteweb.com/license-agreement/  Single domain license
 * @terms of use http://www.absoluteweb.com/terms-of-use/
 */
class AbsoluteWeb_News_Block_Adminhtml_News_Widget_Grid_Column_Renderer_Render
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Longtext
{
    public function render(Varien_Object $row)
    {
        $data = parent::render($row);
        return $this->stripTags($data);
    }
}