<?php
/**
 * Absolute Web Intellectual Property
 *
 * @copyright    Copyright © 1999-2019 Absolute Web, Inc. (http://www.absoluteweb.com)
 * @author       Absolute Web
 * @license      http://www.absoluteweb.com/license-agreement/  Single domain license
 * @terms of use http://www.absoluteweb.com/terms-of-use/
 */

class AbsoluteWeb_News_Block_Adminhtml_News_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/new', array('id' => $row->getEntityId()));
    }

    protected function _construct()
    {
        parent::_construct();

        $this->setDefaultSort('id');
        $this->setId('absoluteweb_news_grid');
        $this->setDefaultDir('asc');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('absoluteweb_news/news')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('entity_id',
            array(
                'header'=> Mage::helper('absoluteweb_news')->__('Id'),
                'type' => 'range',
                'align' =>'right',
                'width' => '50px',
                'index' => 'entity_id',
                'sortable' => true
            )
        );
        $this->addColumn('title',
            array(
                'header'=> Mage::helper('absoluteweb_news')->__('title'),
                'index' => 'title',
                'type' => 'text',
                'sortable'=>true
            )
        );
        $this->addColumn('content',
            array(
                'header'=> Mage::helper('absoluteweb_news')->__('content'),
                'index' => 'content',
                'wysiwyg_enabled' => true,
                'sortable'=>true,
                'type' => 'textarea',
                'renderer' => 'AbsoluteWeb_News_Block_Adminhtml_News_Widget_Grid_Column_Renderer_Render',
                'string_limit' => '255'
            )
        );
        $this->addColumn('create_date',
            array(
                'header'=> Mage::helper('absoluteweb_news')->__('create_Date'),
                'index' => 'create_date',
                'type' => 'datetime',
                'sortable'=>true
            )
        );
        $this->addColumn('update_date',
        array(
            'header'=> Mage::helper('absoluteweb_news')->__('update_Date'),
            'index' => 'update_date',
            'type' => 'datetime',
            'sortable' => true
        )
        );
        $this->addColumn('publish',
            array(
                'header'=> Mage::helper('absoluteweb_news')->__('publish'),
                'index' => 'publish',
                'sortable' => true,
                'type' => 'options',
                'options' => array('1'=>'yes', '0' =>'no')
            )
        );
        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('id');
        $this->getMassactionBlock()->addItem('deleteMass', array(
            'label' => Mage::helper('absoluteweb_news')->__('delete'),
            'url' => $this->getUrl('*/*/deleteMass'),
            'confirm' => Mage::helper('absoluteweb_news')->__('Are you sure?')
        ));
        return $this;
    }

}