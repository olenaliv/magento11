<?php
/**
 * Absolute Web Intellectual Property
 *
 * @copyright    Copyright © 1999-2019 Absolute Web, Inc. (http://www.absoluteweb.com)
 * @author       Absolute Web
 * @license      http://www.absoluteweb.com/license-agreement/  Single domain license
 * @terms of use http://www.absoluteweb.com/terms-of-use/
 */

class AbsoluteWeb_News_Block_Adminhtml_News extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    protected $_blockGroup = 'absoluteweb_news/news';

    protected function _construct()
    {
        $this->_controller = 'adminhtml_news';
        $this->_blockGroup = 'absoluteweb_news';
        $this->_headerText = Mage::helper('absoluteweb_news')->__('Grid');
        parent::_construct();
    }
}