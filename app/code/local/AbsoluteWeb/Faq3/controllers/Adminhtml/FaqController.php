<?php
/**
 * Absolute Web Intellectual Property
 *
 * @copyright    Copyright © 1999-2019 Absolute Web, Inc. (http://www.absoluteweb.com)
 * @author       Absolute Web
 * @license      http://www.absoluteweb.com/license-agreement/  Single domain license
 * @terms of use http://www.absoluteweb.com/terms-of-use/
 */

class AbsoluteWeb_Faq3_Adminhtml_FaqController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Faq grid
     */
    public function indexAction()
    {
        $this->loadLayout();
        /**
         * @todo: set active menu
         */
        $this->_setActiveMenu('absoluteweb_faq3');
        $this->renderLayout();
    }

    /**
     * Edit or create faq
     */
    public function newAction()
    {
        $this->_initFaq();
        $this->loadLayout();
        /**
         * @todo: set active menu and add
         */
        $this->_setActiveMenu('absoluteweb_faq3');
        /**
         * @todo: append faq block (absoluteweb_faq3/adminhtml_faq_edit) to content
         *      (you may find in magento how do it, for example go to Mage -> Adminhtml -> controllers and open CustomerController)
         */
        $this->_addContent(
            $this->getLayout()->createBlock('absoluteweb_faq3/adminhtml_faq_edit', 'edit'),
            $this->getLayout()->createBlock('absoluteweb_faq3/adminhtml_faq_edit_form', 'new')
        );

        $this->renderLayout();
        return $this;
    }

    /**
     * Edit faq action. Forward to new action.
     */
    public function editAction()
    {
        $this->_forward('new');
        $this->_initFaq();
        $this->renderLayout();
    }

    /**
     * Create or save faq.
     */
    public function saveAction()
    {
        /**
         * @todo save to db all params from post
         *      to 'user_id' field set id of the current administrator
         */
        $userId = Mage::getSingleton('admin/session')->getUser()->getUserId();
        $faqObject = Mage::getModel('absoluteweb_faq3/faq');
        $id = $this->getRequest()->getParam('question_id', null);

        if ((!intval($id)) && ($id!=null)) {
            $this->_setErrorMessage('invalid id');
            $this->_redirect('*/*/index');
            return;
        }

        $data = array(
            'question' => $this->getRequest()->getParam('question_enter', ''),
            'answer' => $this->getRequest()->getParam('answer_enter', ''),
            'user_id' => $userId
        );

        if ($id) {
            $faqObject = $faqObject->load($id);
        }

        $faqObject->addData($data)
            ->save();
        $msg = $this->__('The item had been edited');

        if ($faqObject->isObjectNew()) {
            $msg = $this->__('The new item had been added');
        }

        $this->_setSuccessMessage($msg);
        $this->_redirect('*/*/index');
    }

    /**
     * Delete faq action
     */
    public function deleteMassAction()
    {
        /**
         * @todo delete faq by id, use try..catch blocks,
         *          set message about delete process to session and then set redirect to index action
         */
        $requestIds = $this->getRequest()->getParam('id');

        try {
            if (!is_array($requestIds)) {
                Mage::throwException('Please select request(s)');
            }
            foreach ($requestIds as $reqtId) {
                $this->_currentDelete($reqtId);
            }
            $this->_setSuccessMessage( 'Total of %d record(s) were successfully deleted');
        } catch (Exception $e) {
            $this->_setErrorMessage($e->getMessage());
        }

        $this->_redirect('*/*/index');
    }

    public function deleteAction()
    {
        /**
         * @todo delete faq by id, use try..catch blocks,
         *          set message about delete process to session and then set redirect to index action
         */
        $id = $this->getRequest()->getParam('id');

        if ((!intval($id)) && ($id!=null)) {
            $this->_setErrorMessage('invalid id');
            $this->_redirect('*/*/index');
            return;
        }

        try {
            $this->_currentDelete($id);
            $this->_setSuccessMessage( 'Total of %d record(s) were successfully deleted');
        } catch (Exception $e){
            $this->_setErrorMessage($e->getMessage());
        }
        $this->_redirect('*/*/index');
    }

    protected function _initFaq()
    {
        $helper = Mage::helper('absoluteweb_faq3'); // @todo: you must set properly URI of your helper
        $this->_title($helper->__('AbsoluteWeb'))->_title($helper->__('FAQ'));
        /**
         * you can see about register method there http://alanstorm.com/magento_registry_singleton_tutorial
         */

        $model = Mage::getModel('absoluteweb_faq3/faq');
        $faqId = $this->getRequest()->getParam('id');

        if (!is_null($faqId)) {
            $model->load($faqId);
        }
        Mage::register('current_faq', $model);

    }

    private function _setSuccessMessage($msg)
    {
        return Mage::getSingleton('core/session')
            ->addSuccess($this->__($msg));
    }

    private function _setErrorMessage($msg)
    {
        return Mage::getSingleton('core/session')
            ->addError($this->__($msg));
    }

    private function _currentDelete($id)
    {
        Mage::getModel('absoluteweb_faq3/faq')->load($id)->delete();
    }

}
