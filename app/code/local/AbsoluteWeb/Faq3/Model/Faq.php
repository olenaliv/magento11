<?php
/**
 * Absolute Web Intellectual Property
 *
 * @copyright    Copyright © 1999-2019 Absolute Web, Inc. (http://www.absoluteweb.com)
 * @author       Absolute Web
 * @license      http://www.absoluteweb.com/license-agreement/  Single domain license
 * @terms of use http://www.absoluteweb.com/terms-of-use/
 */

class AbsoluteWeb_Faq3_Model_Faq extends Mage_Core_Model_Abstract
{

    public function _construct()
    {
        parent::_construct();
        // @todo: you must init model defined in the config.xml
        $this->_init('absoluteweb_faq3/faq');
    }

}
