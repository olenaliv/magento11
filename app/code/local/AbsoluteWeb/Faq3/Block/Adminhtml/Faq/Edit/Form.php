<?php
/**
 * Absolute Web Intellectual Property
 *
 * @copyright    Copyright © 1999-2019 Absolute Web, Inc. (http://www.absoluteweb.com)
 * @author       Absolute Web
 * @license      http://www.absoluteweb.com/license-agreement/  Single domain license
 * @terms of use http://www.absoluteweb.com/terms-of-use/
 */

class AbsoluteWeb_Faq3_Block_Adminhtml_Faq_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{

    /**
     * Prepare form for render
     */
    protected $_blockGroup = 'absoluteweb_faq3/faq';

    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $faq  = $this->_getModel();
        $helper = Mage::helper('absoluteweb_faq3'); // @todo: you must set properly URI of your helper
        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getUrl('*/*/save'),
            'method' => 'post'
        ));
        $form->setUseContainer(true);
        $this->setForm($form);
        $fieldset = $form->addFieldset('base_fieldset', array('legend' => $helper->__('Create new item')));

        if ($faq->getId()) {
            /**
             * @todo: add field with faq id
             */
            $fieldset->addField('question_id', 'text', array(
                'label' => 'id',
                'name' => 'question_id',
                'value' => $faq->getId()
            ));
        }

        /**
         * @todo: add other fields, use class for validation (js/prototype/validation.js)
         */
        $fieldset->addField('question', 'text', array(
            'label' => Mage::helper('absoluteweb_faq3')->__('Question'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'question_enter'
        ));
        $fieldset->addField('answer', 'text', array(
            'label' => Mage::helper('absoluteweb_faq3')->__('Answer'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'answer_enter'
        ));

        /**
         * @todo: set action to form and other params to form
         */
        $form->addValues($faq->getData());
    }

    private function _getModel()
    {
        $model = Mage::registry('current_faq');
        if (!$model || !($model instanceof AbsoluteWeb_Faq3_Model_Faq)) {
            Mage::throwException('Model is not good');
        }
        return $model;
    }

}