<?php
/**
 * Absolute Web Intellectual Property
 *
 * @copyright    Copyright © 1999-2019 Absolute Web, Inc. (http://www.absoluteweb.com)
 * @author       Absolute Web
 * @license      http://www.absoluteweb.com/license-agreement/  Single domain license
 * @terms of use http://www.absoluteweb.com/terms-of-use/
 */

class AbsoluteWeb_Faq_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     * This method has been created to show output to screen
     *
     * for example you may visit the following URL http://example.com/frontName/index/index/
     * frontName - you must set in config.xml file
     */
    public function indexAction()
    {
        echo 'AbsoluteWeb_Faq Module index';

    }

    /**
     * This method has been created to show output to screen
     *
     * for example you may visit the following URL http://example.com/frontName/index/index2/
     */
    public function index2Action()
    {
        $string = 'AbsoluteWeb_Faq Module index2';

        /**
         * @todo This you should output  $string by using standard magento method to set body
         *       see $this->getResponse()
         */

        $this->getResponse()->setBody($string);
    }

    /**
     * Use this url to send parameter to Controller http://example.com/frontName/index/params?key1=value1&key2=value2
     */
    public function paramsAction()
    {
        $params = array();
        /**
         * @todo here you must replace the empty array by all parameters that have been sent to the Controller
         *       see $this->getRequest()
         */
        $params=$this->getRequest()->getParams();

        echo '<dl>';
        foreach($params as $key => $value){
            echo '<dt><strong>Param: </strong>' . $key . '</dt>';
            echo '<dt><strong>Value: </strong>' . $value . '</dt>';
        }
        echo '</dl>';
    }

    /**
     * Use this url to send parameter to Controller http://example.com/frontName/index/getCustomerById/id/1
     */
    public function getCustomerByIdAction()
    {
        /**
         * @todo acquire ID from URL
         *       that ID will help you to load proper row from DB using model
         */
        $param_id=$this->getRequest()->getParam('id');
        $customerObject = Mage::getModel('customer/customer')->load($param_id);
        var_dump($customerObject->getData());
    }
}