<?php
/**
 * Absolute Web Intellectual Property
 *
 * @copyright    Copyright © 1999-2019 Absolute Web, Inc. (http://www.absoluteweb.com)
 * @author       Absolute Web
 * @license      http://www.absoluteweb.com/license-agreement/  Single domain license
 * @terms of use http://www.absoluteweb.com/terms-of-use/
 */

class AbsoluteWeb_Faq1_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     * For example you may visit the following URL http://example.com/frontName/index/getAllFaq
     */
    /**
      * @todo Load your collection
     */

    public function getAllFaqAction()
    {
        /**
         * @todo Load your collection
         */
        $collection = Mage::getResourceModel('absoluteweb_faq1/faq_collection');
        foreach($collection as $item)
        {
            echo '<h2>' . $item->getQuestion() . '</h2>';
            echo '<p>' . $item->getAnswer() . '</p>';
            echo '<p>' . $item->getDate() . '</p>';
        }
    }

    /**
     * For example you may visit the following URL http://example.com/frontName/index/addNewFaq?question=question1&answer=answer1
     */
    public function addNewFaqAction()
    {
        /**
         * @todo here you must get all params sent by url
         */
        $params = array();
        $params=$this->getRequest()->getParams();

        $faqObject = Mage::getModel('absoluteweb_faq1/faq');
        $faqObject->setData('question', $params['question']);
        $faqObject->setData('answer', $params['answer']);
        /**
         * @todo set params to faq object
         */
        $faqObject->save();

        echo 'New record with ID = ' . $faqObject->getId() . ' successfully added.';
    }

    /**
     * For example you may visit the following URL http://example.com/frontName/index/editFaqById/id/1
     */
    public function editFaqByIdAction()
    {
        /**
         * @todo here you must get id sent by url and load record by id
         */
        $question='Alyona';
        $answer='Livinets';
        $faqObject = Mage::getModel('absoluteweb_faq1/faq');
        $id=$this->getRequest()->getParam('id');
        /**
         * @todo change some field ('question' or 'answer') in object
         */
        $array=array(
            'question' => $question,
            'answer' => $answer,
        );
        $faqObject->load($id)->addData($array)->save();
        echo 'Record with ID = ' . $faqObject->getId() . ' have been changed.';
    }

    /**
     * For example you may visit the following URL http://example.com/frontName/index/deleteFaqById/id/1
     */
    public function deleteFaqByIdAction()
    {
        /**
         * @todo here you must get id sent by url
         */
        $id = $this->getRequest()->getParam('id');

        /**
         * @todo Load model by id
         */
        $faqObject = Mage::getModel('absoluteweb_faq1/faq');
        $faqObject->load($id);

        /**
         * @todo Delete record
         */
        $faqObject->delete();
        echo 'Record with ID = ' . $id . ' have been deleted.';
    }
}